#!/usr/bin/env ruby

require 'elzabmini_gus'

dir = ARGV[0]
year = ARGV[1].to_i

files = ElzabminiGus::select_files dir, year
receipts = ElzabminiGus::collect_receipts files
