# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'elzabmini_gus/version'

Gem::Specification.new do |spec|
  spec.name          = "elzabmini_gus"
  spec.version       = ElzabminiGus::VERSION
  spec.authors       = ["Bartosz Knapik"]
  spec.email         = ["bartosz.knapik@gmail.com"]
  spec.summary       = %q{Simple app for creating monthly / yearly reports related to GUS (Main statistical office) based on elzab mini's daily reports}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_dependency "nokogiri", "~> 1.6.5"
  spec.add_dependency "parallel", "~> 1.3.3"
end
