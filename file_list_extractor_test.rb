require 'minitest/autorun'
require './file_list_extractor'

class FileListExtractorTest < MiniTest::Test
  def setup
    @file_list = File.open('test_index.xml')
    @expected_list = [ "dziennik/0000-0024/BCJ11105751_20121008_113355_0001.xml",
                       "dziennik/0000-0024/BCJ11105751_20121010_130646_0002.xml"]
  end

  def test_extract_and_convert
    assert_equal @expected_list, FileListExtractor.extract(@file_list)
  end

  def test_should_only_take_from_specified_year
    year_break = File.open("year_break.xml")
    list_2012 = ["dziennik/0050-0074/BCJ11105751_20121231_151036_0070.xml",
                 "dziennik/0050-0074/BCJ11105751_20130102_181019_0071.xml"]
    assert_equal list_2012 , FileListExtractor.extract(year_break, year: 2012)
  end

  def test_should_only_take_from_specified_year_2
    year_break = File.open("year_break.xml")
    list_2013 = ["dziennik/0050-0074/BCJ11105751_20130102_181019_0071.xml",
                 "dziennik/0050-0074/BCJ11105751_20130103_175708_0072.xml"]
    assert_equal list_2013 , FileListExtractor.extract(year_break, year: 2013)
  end



end
