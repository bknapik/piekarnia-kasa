require 'nokogiri'

class PriceAndWeightRuleRepository
  def initialize str
    @doc = Nokogiri::XML(str)
  end

  def get_price_unit_mapping
    nodes = @doc.xpath("//entry")
    nodes.inject({}) { |ret, node| ret.merge( (node>"name").text => content_for(node) ) }
  end

  def content_for node
    content = node.xpath("./date").empty? ? :price_and_weight_for : :date_price_and_weight_for
    self.send content, node
  end

  def price_and_weight_for node
    price = (node>"price").text
    weight = (node>"/weight").text
    {unit_price: price, weight: weight}
  end

  def date_price_and_weight_for node
    { date: data_range_and_price_for(node)}
  end

  def data_range_and_price_for node
    node.xpath("./date").map do |date|
      since = date.xpath('./@since').text
      to = date.xpath('./@to').text
      { since: since, to: to, unit_info: price_and_weight_for(date) }
    end
  end
end
