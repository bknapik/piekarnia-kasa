require 'bigdecimal'
require 'date'

class ReportGenerator
  @@tax_rates={ "A" => 1.23, "B" => 1.08, "C" => 1.00, "D" => 1.05 }

  def initialize(data, price_unit_mapping)
    @data = data
    @price_unit_mapping = price_unit_mapping
  end

  def generate(mapping_rule = nil)
    collect_positions(mapping_rule).collect do |name, value|
      create_single_report_element(name, value)
    end
  end

  def collect_positions(mapping_rule)
    collected = Hash.new { |h, k| h[k] = Hash.new(BigDecimal.new("0"))}
    @data.each do |element|
      element["positions"].each do |position|
        collect_positions_by_name(collected, position, mapping_rule, element["date"])
      end
    end
    collected
  end

  def collect_positions_by_name(collected, position, mapping_rule, date)
    original_name = position["name"]
    mapped_name = map_name_according_to original_name, mapping_rule
    total_value = BigDecimal.new(position["val"].sub(",", "."))
    tax = position["vat"]
    amount = calculate_amount(original_name, total_value, date)
    weight = calculate_weight(original_name, amount, date)
    if mapped_name
      collected[mapped_name][:value] += total_value
      collected[mapped_name][:tax] = tax
      collected[mapped_name][:amount] += amount
      collected[mapped_name][:weight] += weight
    end
  end

  def map_name_according_to original_name, mapping_rule
    mapping_rule ? mapping_rule[original_name] : original_name
  end

  def calculate_amount(name, value, date)
    value / extract_mapping(name, :unit_price, date)
  end

  def calculate_weight(name, amount, date)
    amount * extract_mapping(name, :weight, date)
  end

  def extract_mapping(name, symbol, date)
    if (@price_unit_mapping[name].key?(:date))
      BigDecimal.new(select_rule_for_date(name, date)[0][:unit_info][symbol].sub(",", "."))
    else
      BigDecimal.new(@price_unit_mapping[name][symbol].sub(",", "."))
    end
  end

  def select_rule_for_date(name, receipt_date)
    @price_unit_mapping[name][:date].select do |date|
      since = Date.strptime(date[:since], '%d.%m.%Y')
      to = Date.strptime(date[:to], '%d.%m.%Y')
      receipt = Date.strptime(receipt_date, '%d.%m.%Y')

      (since..to).cover? receipt
    end
  end

  def calculate_netto value, vat_letter
    value / @@tax_rates[vat_letter]
  end

  def print_two_digits value
    sprintf("%#.2f", value.to_f()).gsub(".", ",")
  end

  def create_single_report_element name, value_with_tax
    {
      :name => name,
      :netto => print_two_digits(calculate_netto(value_with_tax[:value], value_with_tax[:tax])),
      :brutto => print_two_digits(value_with_tax[:value]),
      :amount => print_two_digits(value_with_tax[:amount]),
      :weight => print_two_digits(value_with_tax[:weight])
    }
  end

end
