require('nokogiri')

class ReceiptReader
  # returns array of hashes { date, time, positions:[ { name, val, vat } ] }
  def parse(str)
    document = MyDocument.new
    parser = Nokogiri::XML::SAX::Parser.new(document)
    parser.parse(str)

    document.receipts.each do |receipt|
      remove_voided receipt["positions"]
      omit_type receipt["positions"]
    end
  end

  def remove_voided(positions)
    void_positions = positions.select { |position| position["type"] == "void" }

    void_positions.each do |position|
      remove_single_position_from(create_voided_from_void(position), positions)
    end

    positions.delete_if { |position| position["type"] == "void" }
  end

  def create_voided_from_void(position)
    voided_position = position.dup
    voided_position["type"] = "sale"
    voided_position
  end

  def remove_single_position_from(voided_position, positions)
      index = positions.index(voided_position)
      positions.delete_at index unless index.nil?
  end

  def omit_type(positions)
    positions.collect { |position| position.delete_if { |k,v| k =="type" } }
  end

end

class MyDocument < Nokogiri::XML::SAX::Document
  attr_accessor :receipts

  def initialize
    @receipts = []
    @is_receipt = false
    @is_vat  = false
    @is_name = false
    @is_type = false
    @is_val  = false
    @is_date = false
    @is_time = false
  end

  def start_element name, attributes = []
    if name.eql? "Action" and attributes.include?(["Canceled", "N"]) and attributes.include?(["PrintoutName", "rcpt"])
      @is_receipt = true
      @receipts << { "date" => "", "time" => "", "positions" => [] }
    end

    if (@is_receipt)
      if name.eql? "itmvat"
        @receipts[-1]["positions"] << { "name" => "", "val" => "", "vat" => "", "type" => "" }
        @is_vat = true
      end

      @is_name = true if name.eql? "itmname"
      @is_type = true if name.eql? "itmtype"
      @is_val  = true if name.eql? "itmval"
      @is_date = true if name.eql? "po.date"
      @is_time = true if name.eql? "po.time"
    end
  end

  def end_element name
    @is_receipt = false if name.eql? "Action"
    @is_vat  = false if name.eql? "itmvat"
    @is_name = false if name.eql? "itmname"
    @is_type = false if name.eql? "itmtype"
    @is_val  = false if name.eql? "itmval"
    @is_date = false if name.eql? "po.date"
    @is_time = false if name.eql? "po.time"
  end

  def characters str
    @receipts[-1]["positions"][-1]["vat"] = str if (@is_vat)
    @receipts[-1]["positions"][-1]["name"] += str.strip if (@is_name)
    @receipts[-1]["positions"][-1]["type"] = str if (@is_type)
    @receipts[-1]["positions"][-1]["val"] = str if (@is_val)
    @receipts[-1]["date"] = str if (@is_date)
    @receipts[-1]["time"] = str if (@is_time)
  end
end
