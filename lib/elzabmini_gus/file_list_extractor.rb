require 'nokogiri'

module FileListExtractor
  def FileListExtractor.extract(str, options = {})
    doc = Nokogiri::XML(str)
    path = "//FileItem"
    if options.key? :year
      year = options[:year]
      path = "//FileItem[contains(@begin.date, '#{year}') or contains(@end.date, '#{year}')]"
    end
    files = doc.xpath(path)
    names = files.collect { |file| file.attr('FileName') }
    names.collect { |name| name.gsub("\\", "/") }
  end
end
