require "elzabmini_gus/version"
require 'elzabmini_gus/file_list_extractor'
require 'elzabmini_gus/receipt_reader'
require 'parallel'

module ElzabminiGus
  def ElzabminiGus.select_files dir, year
    file_list = File.open("#{dir}/index.xml") { |f| FileListExtractor.extract(f, year: year) }
    file_list.collect! { |file| "#{dir}/#{file}" }
  end

  def ElzabminiGus.collect_receipts file_list
    receipt_reader = ReceiptReader.new
    receipts = Parallel.map_with_index(file_list) do |file, i|
      puts "Progress: #{i}/#{file_list.size}"
      File.open(file) { |f| receipt_reader.parse(f) }
    end
  end
end
