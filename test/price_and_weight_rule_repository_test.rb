require 'minitest/autorun'
require 'elzabmini_gus/price_and_weight_rule_repository'

class PriceAndWeightRuleRepositoryTest < MiniTest::Test
  def setup
    @empty = ""
    @single_mapping_rule = <<EOXML
    <prices_units>
      <entry>
        <name>\"BUŁKA ZWYKŁA\"</name>
        <price>1,20</price>
        <weight>0,10</weight>
      </entry>
    </prices_units>
EOXML
    @multiple_mapping_rules = <<EOXML
    <prices_units>
       <entry>
        <name>\"BUŁKA ZWYKŁA\"</name>
        <price>1,20</price>
        <weight>0,10</weight>
      </entry>
      <entry>
        <name>\"BUŁKA NIEZWYKŁA\"</name>
        <price>2,20</price>
        <weight>0,30</weight>
      </entry>
    </prices_units>
EOXML
  end

  def test_should_get_no_rules
    mapping_rules = PriceAndWeightRuleRepository.new @empty
    assert_equal({}, mapping_rules.get_price_unit_mapping)
  end

  def test_should_take_single_price_rule
   mapping_rules = PriceAndWeightRuleRepository.new @single_mapping_rule
    assert mapping_rules.get_price_unit_mapping.key? "\"BUŁKA ZWYKŁA\""
    assert mapping_rules.get_price_unit_mapping.value?({ unit_price: "1,20" , weight: "0,10"})
    assert_equal({ "\"BUŁKA ZWYKŁA\"" => { unit_price: "1,20" , weight: "0,10"} }, mapping_rules.get_price_unit_mapping)
  end

  def test_should_take_multiple_price_rules
    mapping_rules = PriceAndWeightRuleRepository.new @multiple_mapping_rules
    assert mapping_rules.get_price_unit_mapping.key? "\"BUŁKA ZWYKŁA\""
    assert mapping_rules.get_price_unit_mapping.value?({ unit_price: "1,20" , weight: "0,10"})

    assert mapping_rules.get_price_unit_mapping.key? "\"BUŁKA NIEZWYKŁA\""
    assert mapping_rules.get_price_unit_mapping.value?({ unit_price: "2,20" , weight: "0,30"})

    assert_equal({ "\"BUŁKA ZWYKŁA\"" => { unit_price: "1,20" , weight: "0,10"},
                   "\"BUŁKA NIEZWYKŁA\"" => { unit_price: "2,20" , weight: "0,30"}}, mapping_rules.get_price_unit_mapping)

  end

  def test_should_add_date_range
    with_date_range = <<EOXML
    <prices_units>
      <entry>
        <name>\"BUŁKA ZWYKŁA\"</name>
        <date since="01.01.2014" to="02.01.2014">
          <price>1,20</price>
          <weight>0,10</weight>
        </date>
      </entry>
    </prices_units>
EOXML

    mapping_rules = PriceAndWeightRuleRepository.new with_date_range
    price_unit_mapping = { "\"BUŁKA ZWYKŁA\""     =>
                            { date: [ {since: "01.01.2014",
                                       to: "02.01.2014",
                                       unit_info: { unit_price: "1,20" , weight: "0,10"}} ] } }
    assert_equal price_unit_mapping, mapping_rules.get_price_unit_mapping
  end

  def test_should_add_multiple_date_ranges
    with_date_range = <<EOXML
    <prices_units>
      <entry>
        <name>\"BUŁKA ZWYKŁA\"</name>
        <date since="01.01.2014" to="02.01.2014">
          <price>1,20</price>
          <weight>0,10</weight>
        </date>
        <date since="03.01.2014" to="05.01.2014">
          <price>1,40</price>
          <weight>0,10</weight>
        </date>
      </entry>
    </prices_units>
EOXML

    mapping_rules = PriceAndWeightRuleRepository.new with_date_range
    price_unit_mapping = { "\"BUŁKA ZWYKŁA\""     =>
                            { date: [ {since: "01.01.2014",
                                       to: "02.01.2014",
                                       unit_info: { unit_price: "1,20" , weight: "0,10"}},
                                       {since: "03.01.2014",
                                       to: "05.01.2014",
                                       unit_info: { unit_price: "1,40" , weight: "0,10"}}] } }
    assert_equal price_unit_mapping, mapping_rules.get_price_unit_mapping
  end

end
