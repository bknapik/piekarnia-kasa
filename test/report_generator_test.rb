require 'minitest/autorun'
require 'elzabmini_gus/report_generator'

class ReportGeneratorTest < MiniTest::Test
  def setup
    @data = [
        {"date"=>"02.01.2014", "time"=>"04:10:00", "positions"=>[{"name"=>"\"BUŁKA ZWYKŁA\"", "val"=>"1,20", "vat"=>"D"}]},
        {"date"=>"02.01.2014", "time"=>"05:46:00", "positions"=>[{"name"=>"\"BUŁKA MAŁA\"", "val"=>"1,40", "vat"=>"D"},
                                                                 {"name"=>"\"CHLEB KROJONY MA\"", "val"=>"3,10", "vat"=>"D"}]},
        {"date"=>"02.01.2014", "time"=>"04:10:00", "positions"=>[{"name"=>"\"BUŁKA NIEZWYKŁA\"", "val"=>"1,23", "vat"=>"A"}]}]
    @price_unit_mapping = { "\"BUŁKA ZWYKŁA\""     => { unit_price: "1,20" , weight: "0,10"},
                            "\"BUŁKA MAŁA\""       => { unit_price: "1,40" , weight: "0,2"},
                            "\"CHLEB KROJONY MA\"" => { unit_price: "3,10" , weight: "0,33"},
                            "\"BUŁKA NIEZWYKŁA\""  => { unit_price: "1,23" , weight: "0,5"}}
    @rg = ReportGenerator.new(@data, @price_unit_mapping)
  end

  def test_generate_report
    expected_record = [ { name: "\"BUŁKA ZWYKŁA\"",netto: "1,14", brutto: "1,20", amount: "1,00", weight: "0,10"},
                        { name: "\"BUŁKA MAŁA\"", netto: "1,33", brutto: "1,40", amount: "1,00", weight: "0,20" },
                        { name: "\"CHLEB KROJONY MA\"", netto: "2,95", brutto:"3,10", amount: "1,00", weight: "0,33"  },
                        { name: "\"BUŁKA NIEZWYKŁA\"", netto: "1,00", brutto: "1,23", amount: "1,00", weight: "0,50"  } ]
    assert_equal expected_record, @rg.generate()
  end

  def test_generate_report_with_empty_mapping_rule
    rule = { }
    empty_record = []
    assert_equal empty_record, @rg.generate(rule)
  end

  def test_generate_report_with_elements_from_mapping_rule
    rule = { "\"BUŁKA ZWYKŁA\"" => "Pieczywo" }
    expected_record = [{ name: "Pieczywo",netto: "1,14", brutto: "1,20", amount: "1,00", weight: "0,10" }]
    assert_equal expected_record, @rg.generate(rule)
  end

  def test_generate_report_and_squash_elements_from_mapping_rule
    rule = { "\"BUŁKA ZWYKŁA\"" => "Pieczywo", "\"BUŁKA MAŁA\"" => "Pieczywo"}
    expected_record = [{ name: "Pieczywo",netto: "2,48", brutto: "2,60", amount: "2,00", weight: "0,30" }]
    assert_equal expected_record, @rg.generate(rule)
  end

  def test_generate_report_with_date_pricing_rule
    @data = [
        {"date"=>"02.01.2014", "time"=>"04:10:00", "positions"=>[{"name"=>"\"BUŁKA ZWYKŁA\"", "val"=>"1,20", "vat"=>"D"}]},
        {"date"=>"04.01.2014", "time"=>"04:10:00", "positions"=>[{"name"=>"\"BUŁKA ZWYKŁA\"", "val"=>"1,20", "vat"=>"D"}]}
        ]
    price_unit_mapping = { "\"BUŁKA ZWYKŁA\""     =>
                            { date: [ {since: "01.01.2014",
                                       to: "02.01.2014",
                                       unit_info: { unit_price: "1,20" , weight: "0,10"}},
                                      {since: "03.01.2014",
                                       to: "31.12.2014",
                                       unit_info: {unit_price: "1,20", weight: "0,15"}}]}}

    @rg = ReportGenerator.new(@data, price_unit_mapping)

    rule = { "\"BUŁKA ZWYKŁA\"" => "Pieczywo" }
    expected_record = [{ name: "Pieczywo",netto: "2,29", brutto: "2,40", amount: "2,00", weight: "0,25" }]
    assert_equal expected_record, @rg.generate(rule)
  end

end
