require('minitest/autorun')
require 'elzabmini_gus/receipt_reader'

class ReceiptReaderTest < MiniTest::Test

  def setup
    @with_void = <<EOXML
    <Action PrintoutName="rcpt" BeginPosition="8895" EndPosition="9478"    Canceled="N">
      <itmvat>D</itmvat>
      <itmname>&quot;BUŁKA ZWYKŁA&quot;</itmname>
      <itmtype>sale</itmtype>
      <itmval>1,20</itmval>
      <itmvat>D</itmvat>
      <itmname>&quot;BUŁKA ZWYKŁA&quot;</itmname>
      <itmtype>void</itmtype>
      <itmval>1,20</itmval>
      <itmvat>D</itmvat>
      <itmname>&quot;BUŁKA ZWYKŁA&quot;</itmname>
      <itmtype>sale</itmtype>
      <itmval>1,20</itmval>
      <itmvat>B</itmvat>
      <itmname>&quot;ROGAL FIRMOWY&quot;</itmname>
      <itmtype>sale</itmtype>
      <itmval>2,40</itmval>
      <sign>6F733F5381A0DCBCCA5B1D7CFDD3A425</sign>
      <tra.tot.v>+0,00/+2,40/+0,00/+1,20/+0,00/+0,00/+0,00</tra.tot.v>
      <day_n.rcpt_clos>5</day_n.rcpt_clos>
      <day_n.rcpt_canc>0</day_n.rcpt_canc>
      <day_n.printouts>9</day_n.printouts>
      <grn_n.printouts>7145</grn_n.printouts>
      <po.date>22.12.2014</po.date>
      <po.time>06:09:00</po.time>
      <cs_po.sha-e>#98BB805FF7E2859411ED40613BD81375A826A397</cs_po.sha-e>
    </Action>
EOXML
    @expected_with_void =
    [
      {
        "date"=>"22.12.2014",
        "time"=>"06:09:00",
        "positions"=>
        [
          {
            "name"=>"\"BUŁKA ZWYKŁA\"",
            "val"=>"1,20",
            "vat"=>"D"
          },
          {
            "name"=>"\"ROGAL FIRMOWY\"",
            "val"=>"2,40",
            "vat"=>"B"
          }
        ]
      }
    ]
  end

  def test_parsing_with_void
    assert_equal @expected_with_void, ReceiptReader.new.parse(@with_void)
  end

end
